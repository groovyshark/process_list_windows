#using <System.dll>

using namespace System;
using namespace System::Diagnostics;
using namespace System::ComponentModel;


int main()
{
	array <Process^> ^processList = Process::GetProcesses();

	Console::WriteLine("{0,-35} {1,-5} {2,-10} {3}", "NAME", "PID", "THREADS", "PATHTOEXE");
	for each(Process^ process  in processList)
	{
		String^ name = process->ProcessName;
		int pid = process->Id;
		int threadsCount = process->Threads->Count;
		String^ pathToExe = name; 

		try
		{
			pathToExe = process->MainModule->FileName;
		}
		catch (Exception^ w)
		{
			pathToExe += ", error: " + w->Message;
		}
		Console::WriteLine("{0,-35} {1,-5} {2,-10} {3}", name, pid, threadsCount, pathToExe);
	}
	Console::WriteLine("Process count: {0}", processList->Length);
	Console::ReadLine();
	
	return 0;
}